/*
The jbond command line tool translates bonds and json.
*/

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/vguarnaccia/dez/fixedincome"
)

var (
	printExample bool
	iFile        string
)

func init() {
	flag.BoolVar(&printExample, "example", false, "Print an example bond")
	flag.StringVar(&iFile, "i", "bonds.json", "Input file path for bonds.")
}

func main() {
	flag.Parse()
	if printExample {
		example := fixedincome.Bond{}
		oJSON, _ := json.Marshal(example)
		fmt.Println(string(oJSON[:]))
		return
	}
	iJSON, err := ioutil.ReadFile(iFile)
	if err != nil {
		log.Fatalf("Could not read from %v: %v", iFile, err)
	}
	var bond fixedincome.Bond
	if err := json.Unmarshal(iJSON, &bond); err != nil {
		log.Fatalf("Could not unmarshall json: %v", err)
	}
	fixedincome.PPrint(bond.Cashflows(fixedincome.EmptyTermStructure{}))

}
