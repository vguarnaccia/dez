package fixedincome

import (
	"math"
	"reflect"
	"testing"
	"time"
)

func TestBond_PaymentDates(t *testing.T) {
	type fields struct {
		SettlementDate       time.Time
		SettlementAmount     float64
		MaturityDate         time.Time
		DayCountConvention   DayCountConvention
		PaymentFrequency     Frequency
		CompoundingFrequency Frequency
		FirstPaymentDate     time.Time
		AnnuityDates         []time.Time
		AnnuityPayments      map[time.Time]float64
		Principals           map[time.Time]float64
		EndOfMonthPayer      bool
	}
	tests := []struct {
		name   string
		fields fields
		want   []time.Time
	}{
		{
			"Simple zero coupon bond",
			fields{
				SettlementDate:   SimpleDate("2013-01-15"),
				SettlementAmount: 100.00,
				MaturityDate:     SimpleDate("2015-01-15"),
				PaymentFrequency: ZeroCoupon,
			},
			isoDates("2015-01-15"),
		},
		{
			"Simple semiannual coupon bond",
			fields{
				SettlementDate:   SimpleDate("2010-05-15"),
				MaturityDate:     SimpleDate("2012-05-15"),
				PaymentFrequency: Semiannually,
			},
			isoDates("2010-11-15", "2011-05-15", "2011-11-15", "2012-05-15"),
		},
		{
			"Monthly coupon bond with irregular maturity.",
			fields{
				SettlementDate:   SimpleDate("2001-01-01"),
				MaturityDate:     SimpleDate("2001-12-31"),
				PaymentFrequency: Monthly,
			},
			isoDates(
				"2001-02-01",
				"2001-03-01",
				"2001-04-01",
				"2001-05-01",
				"2001-06-01",
				"2001-07-01",
				"2001-08-01",
				"2001-09-01",
				"2001-10-01",
				"2001-11-01",
				"2001-12-01",
				"2001-12-31",
			),
		},
		{
			"Bimonthly coupon bond with EOM payments.",
			fields{
				SettlementDate:   SimpleDate("2001-01-01"),
				MaturityDate:     SimpleDate("2001-12-31"),
				PaymentFrequency: Bimonthly,
				EndOfMonthPayer:  true,
			},
			isoDates(
				"2001-03-31",
				"2001-05-31",
				"2001-07-31",
				"2001-09-30",
				"2001-11-30",
				"2001-12-31",
			),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &Bond{
				SettlementDate:       tt.fields.SettlementDate,
				SettlementAmount:     tt.fields.SettlementAmount,
				MaturityDate:         tt.fields.MaturityDate,
				DayCountConvention:   tt.fields.DayCountConvention,
				PaymentFrequency:     tt.fields.PaymentFrequency,
				CompoundingFrequency: tt.fields.CompoundingFrequency,
				FirstPaymentDate:     tt.fields.FirstPaymentDate,
				EndOfMonthPayer:      tt.fields.EndOfMonthPayer,
			}
			b.PaymentDates()
			if !reflect.DeepEqual(b.Dates, tt.want) {
				t.Errorf("AnnuityDates = %v, want %v", b.Dates, tt.want)
			}
		})
	}
}

func TestBond_getCoupon(t *testing.T) {
	tests := []struct {
		name    string
		Coupons []Coupon
		date    time.Time
		want    Coupon
	}{
		{
			"Single coupon",
			[]Coupon{Coupon{Effective: SimpleDate("2015-04-10")}},
			SimpleDate("2017-01-01"),
			Coupon{Effective: SimpleDate("2015-04-10")},
		},
		{
			"Get last coupon",
			[]Coupon{
				Coupon{Effective: SimpleDate("2015-04-10")},
				Coupon{Effective: SimpleDate("2016-04-10")},
			},
			SimpleDate("2017-01-01"),
			Coupon{Effective: SimpleDate("2016-04-10")},
		},
		{
			"Get middle coupon",
			[]Coupon{
				Coupon{Effective: SimpleDate("2015-04-10")},
				Coupon{Effective: SimpleDate("2016-04-10")},
				Coupon{Effective: SimpleDate("2017-04-10")},
			},
			SimpleDate("2017-01-01"),
			Coupon{Effective: SimpleDate("2016-04-10")},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := Bond{Coupons: tt.Coupons}
			if got := b.getCoupon(tt.date); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Bond.getCoupon() = %v, want %v", got, tt.want)
			}
		})
	}
}

func CompareCashflows(got, want []Cashflow) bool {
	if len(got) != len(want) {
		return false
	}
	tolerance := 1e-15
	for i := 0; i < len(got); i++ {
		if !got[i].Date.Equal(want[i].Date) ||
			math.Abs(want[i].Interest-got[i].Interest) > tolerance ||
			math.Abs(want[i].Principal-got[i].Principal) > tolerance ||
			math.Abs(want[i].RemainingPrincipal-got[i].RemainingPrincipal) > tolerance {
			return false
		}
	}
	return true
}

func TestBond_Cashflows(t *testing.T) {
	type fields struct {
		SettlementDate       time.Time
		SettlementAmount     float64
		MaturityDate         time.Time
		DayCountConvention   DayCountConvention
		PaymentFrequency     Frequency
		CompoundingFrequency Frequency
		FirstPaymentDate     time.Time
		PercentResidual      float64
		EndOfMonthPayer      bool
		Coupons              []Coupon
		Dates                []time.Time
	}
	tests := []struct {
		name         string
		fields       fields
		termStructer TermStructerer
		want         []Cashflow
	}{
		{
			"Trivial zero-coupon bond",
			fields{
				SettlementDate:       SimpleDate("2001-01-01"),
				SettlementAmount:     100.00,
				MaturityDate:         SimpleDate("2001-12-31"),
				PaymentFrequency:     ZeroCoupon,
				CompoundingFrequency: Semiannually,
				Coupons: []Coupon{
					{
						Index:      FixedRate,
						Spread:     0.00,
						Amortizing: false,
						Effective:  SimpleDate("2001-01-01"),
					},
				},
			},
			EmptyTermStructure{},
			[]Cashflow{
				{
					Date:               SimpleDate("2001-12-31"),
					Interest:           0.00,
					Principal:          100.00,
					RemainingPrincipal: 0.00,
				},
			},
		},
		{
			"Simple zero-coupon bond",
			fields{
				SettlementDate:       SimpleDate("2001-01-15"),
				SettlementAmount:     100.00,
				MaturityDate:         SimpleDate("2002-01-15"),
				PaymentFrequency:     ZeroCoupon,
				CompoundingFrequency: Semiannually,
				Coupons: []Coupon{
					{
						Index:      FixedRate,
						Spread:     0.10,
						Amortizing: false,
						Effective:  SimpleDate("2001-01-15"),
					},
				},
			},
			EmptyTermStructure{},
			[]Cashflow{
				{
					Date:               SimpleDate("2002-01-15"),
					Interest:           100.00 * (math.Pow(1+0.10/2, 2) - 1),
					Principal:          100.00,
					RemainingPrincipal: 0.00,
				},
			},
		},
		{
			"Simple IO bond",
			fields{
				SettlementDate:       SimpleDate("2001-01-15"),
				SettlementAmount:     100.00,
				MaturityDate:         SimpleDate("2002-01-15"),
				DayCountConvention:   Thirty360,
				PaymentFrequency:     Semiannually,
				CompoundingFrequency: Semiannually,
				Coupons: []Coupon{
					{
						Index:      FixedRate,
						Spread:     0.02,
						Amortizing: false,
						Effective:  SimpleDate("2001-01-15"),
					},
				},
			},
			EmptyTermStructure{},
			[]Cashflow{
				{
					Date:               SimpleDate("2001-07-15"),
					Interest:           1.0,
					Principal:          0.00,
					RemainingPrincipal: 100.00,
				},
				{
					Date:               SimpleDate("2002-01-15"),
					Interest:           1.0,
					Principal:          100.00,
					RemainingPrincipal: 0.00,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := Bond{
				SettlementDate:       tt.fields.SettlementDate,
				SettlementAmount:     tt.fields.SettlementAmount,
				MaturityDate:         tt.fields.MaturityDate,
				DayCountConvention:   tt.fields.DayCountConvention,
				PaymentFrequency:     tt.fields.PaymentFrequency,
				CompoundingFrequency: tt.fields.CompoundingFrequency,
				FirstPaymentDate:     tt.fields.FirstPaymentDate,
				PercentResidual:      tt.fields.PercentResidual,
				EndOfMonthPayer:      tt.fields.EndOfMonthPayer,
				Coupons:              tt.fields.Coupons,
				Dates:                tt.fields.Dates,
			}
			if got := b.Cashflows(tt.termStructer); !CompareCashflows(got, tt.want) {
				t.Errorf("Bond.Cashflows() = %+v, want %+v", got, tt.want)
			}
		})
	}
}
