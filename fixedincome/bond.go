/*
Package fixedincome calculates cash flows for fixed income securities.
*/
package fixedincome

import (
	"log"
	"math"
	"time"
)

// Index is a enum for interest indices.
type Index int

// Indices that can be supplied to a term structure.
const (
	FixedRate Index = iota // Index for fixed rate instrument (i.e. 0%)
)

// TermStructerer is an object that can return interest rates.
type TermStructerer interface {
	Rate(Index, time.Time) float64
}

// EmptyTermStructure contains no rates.
type EmptyTermStructure struct{}

// Rate implemented for only FixedRate bonds.
func (d EmptyTermStructure) Rate(idx Index, marketDate time.Time) float64 {
	if idx != FixedRate {
		log.Panicln("Dummy term structure has no market date.")
	}
	return 0
}

// DayCountConvention indicates different ways ot accruing interest.
//
// https://en.wikipedia.org/wiki/Day_count_convention
type DayCountConvention int

// A few common day count conventions.
//
// [1]: https://en.wikipedia.org/wiki/Day_count_convention
//
// [2]: https://web.archive.org/web/20150924031602/http://www.hsbcnet.com/gbm/attachments/standalone/2006-isda-definitions.pdf
const (
	Thirty360 DayCountConvention = iota // 30E/360 Eurobonds
	ActAct                              // actual/actual ISDA
	Act360                              // actual/360
	Act365                              // actual/365
)

// Frequency indicates how many times a year a bond pays or compounds.
type Frequency int

// Common frequencies. All of divisors of 12.
const (
	ZeroCoupon Frequency = iota // No annuity payments.
	Annually
	Semiannually // Twice a year.
	Trimonthly
	Quarterly
	// Every two months, not twice a month (semimonthly).
	Bimonthly = 6
	Monthly   = 12
)

// Coupon contains fields necessary to calculate payments
type Coupon struct {
	Index      Index
	Spread     float64
	Amortizing bool // Whether bond amortizes in period
	Effective  time.Time
}

// Cashflow records balances at a point in time.
type Cashflow struct {
	Date               time.Time
	Interest           float64
	Principal          float64
	RemainingPrincipal float64
}

// Bond represents a simple bond instrument.
type Bond struct {
	SettlementDate       time.Time
	SettlementAmount     float64
	MaturityDate         time.Time
	DayCountConvention   DayCountConvention
	PaymentFrequency     Frequency
	CompoundingFrequency Frequency `json:",omitempty"`
	FirstPaymentDate     time.Time `json:",omitempty"`
	PercentResidual      float64   // Percent remaining balance at maturity
	EndOfMonthPayer      bool

	Coupons []Coupon

	Dates []time.Time `json:"-"`
}

// Find last relevant coupon. Assumes number of coupons is small.
func (b Bond) getCoupon(date time.Time) Coupon {
	priorCoupon := b.Coupons[0]
	for _, nextCoupon := range b.Coupons {
		if nextCoupon.Effective.After(date) {
			return priorCoupon
		}
		priorCoupon = nextCoupon
	}
	return priorCoupon
}

func (b Bond) nextPayment(startDate, endDate time.Time, UPB float64,
	termStructure TermStructerer) (principal, interest float64) {

	coupon := b.getCoupon(startDate)
	payments := float64(len(b.Dates))
	AnnualRate := termStructure.Rate(coupon.Index, startDate) + coupon.Spread
	dayCountFactor := DayCountFactor(startDate, endDate, b.DayCountConvention)
	compounds := float64(b.CompoundingFrequency)
	rate := math.Pow(1+AnnualRate/compounds, dayCountFactor*compounds) - 1
	balloon := b.SettlementAmount * (1 - b.PercentResidual)
	switch {

	case !coupon.Amortizing:
		interest = UPB * rate
		if endDate == b.MaturityDate {
			principal = UPB
		}
		return

	case AnnualRate == 0:
		principal = UPB / payments
		return

	case b.DayCountConvention == Thirty360:
		rate := AnnualRate / float64(b.PaymentFrequency)

		// Split bond into amortizing and non-amortizing strips.
		amortizingPayment := (UPB - balloon) * (rate + rate/(math.Pow(1+rate, payments)-1))
		balloonInterest := balloon * rate
		totalPayment := amortizingPayment + balloonInterest

		interest = UPB * rate
		principal = totalPayment - interest
		if endDate == b.MaturityDate {
			principal += balloon
		}
		return

	default:
		log.Panicf("Amortization for %v day count convention not yet implemented.", b.DayCountConvention)
		return
	}
}

// Cashflows gets all cashflows for a given bond.
func (b Bond) Cashflows(termStructer TermStructerer) []Cashflow {
	if b.Dates == nil {
		b.PaymentDates()
	}
	payments := len(b.Dates)
	UPB := b.SettlementAmount
	cashflows := make([]Cashflow, payments)
	now := b.SettlementDate
	var principal, interest float64
	for i := 0; i < len(b.Dates); i++ {
		next := b.Dates[i]
		principal, interest = b.nextPayment(now, next, UPB, termStructer)
		UPB -= principal
		cashflows[i] = Cashflow{
			Date:               next,
			Interest:           interest,
			Principal:          principal,
			RemainingPrincipal: UPB,
		}
		now = next
	}
	return cashflows
}

// PaymentDates populates list of all payment dates.
func (b *Bond) PaymentDates() {
	if b.PaymentFrequency == ZeroCoupon {
		b.Dates = append(b.Dates, b.MaturityDate)
		return
	}
	var nextPayment func(time.Time, int) time.Time
	switch b.EndOfMonthPayer {
	case true:
		nextPayment = EndOfMonth
	case false:
		nextPayment = EDate
	}
	b.Dates = make([]time.Time, 0)

	couponFactor := int(12 / b.PaymentFrequency)
	if b.FirstPaymentDate.IsZero() {
		b.FirstPaymentDate = nextPayment(b.SettlementDate, couponFactor)
	}
	b.Dates = append(b.Dates, b.FirstPaymentDate)
	next := nextPayment(b.FirstPaymentDate, couponFactor)
	for next.Before(b.MaturityDate) {
		b.Dates = append(b.Dates, next)
		next = nextPayment(next, couponFactor)
	}
	b.Dates = append(b.Dates, b.MaturityDate)
}
