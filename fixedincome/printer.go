package fixedincome

import (
	"fmt"
	"os"
	"text/tabwriter"
)

// PPrint pretty prints cashflows
func PPrint(cashflows []Cashflow) {
	w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ',
		tabwriter.AlignRight)
	fmt.Fprintf(w,
		"%v\t%v\t%v\t%v\t\n",
		"Date",
		"RemaingPrincipal",
		"Interest",
		"Principal",
	)
	for _, cashflow := range cashflows {
		fmt.Fprintf(w,
			"%v\t%.2f\t%.2f\t%.2f\t\n",
			cashflow.Date.Format("2006-01-02"),
			cashflow.RemainingPrincipal,
			cashflow.Interest,
			cashflow.Principal,
		)
	}
	w.Flush()
}
