package fixedincome

import (
	"reflect"
	"testing"
	"time"
)

func TestEDate(t *testing.T) {
	type args struct {
		date   time.Time
		months int
	}
	tests := []struct {
		name string
		args args
		want time.Time
	}{
		{"Same year forward", args{SimpleDate("2000-01-30"), 5}, SimpleDate("2000-06-30")},
		{"Same year backward", args{SimpleDate("2000-06-30"), -5}, SimpleDate("2000-01-30")},
		{"Different year forward", args{SimpleDate("2010-07-15"), 10}, SimpleDate("2011-05-15")},
		{"Across February", args{SimpleDate("2010-01-30"), 1}, SimpleDate("2010-02-28")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EDate(tt.args.date, tt.args.months); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EDate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEndOfMonth(t *testing.T) {
	type args struct {
		date   time.Time
		months int
	}
	tests := []struct {
		name string
		args args
		want time.Time
	}{
		{"Same year forward", args{SimpleDate("2000-01-30"), 5}, SimpleDate("2000-06-30")},
		{"Same year backward", args{SimpleDate("2000-06-30"), -5}, SimpleDate("2000-01-31")},
		{"Different year forward", args{SimpleDate("2010-07-15"), 10}, SimpleDate("2011-05-31")},
		{"Across February", args{SimpleDate("2010-01-30"), 1}, SimpleDate("2010-02-28")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EndOfMonth(tt.args.date, tt.args.months); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EndOfMonth() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDayCountFactor(t *testing.T) {
	type args struct {
		accrualStart time.Time
		accrualEnd   time.Time
		basis        DayCountConvention
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			"Simple 30/360",
			args{
				SimpleDate("2003-01-04"),
				SimpleDate("2003-02-04"),
				Thirty360,
			},
			30 / 360.,
		},
		{
			"Simple actual/actual not leap year",
			args{
				SimpleDate("2018-01-15"),
				SimpleDate("2018-06-15"),
				ActAct,
			},
			151 / 365.,
		},
		{
			"Simple actual/actual leap year",
			args{
				SimpleDate("2020-01-15"),
				SimpleDate("2020-06-15"),
				ActAct,
			},
			152 / 366.,
		},
		{
			"Simple actual/365",
			args{
				SimpleDate("2020-01-15"),
				SimpleDate("2020-06-15"),
				Act365,
			},
			152 / 365.,
		},
		{
			"Simple actual/360",
			args{
				SimpleDate("2020-01-15"),
				SimpleDate("2020-06-15"),
				Act360,
			},
			152 / 360.,
		},
		{
			"Different non-leap years actual/actual",
			args{
				SimpleDate("2015-01-15"),
				SimpleDate("2016-06-15"),
				Act365,
			},
			(350 + 167) / 365.,
		},
		{
			"Long non-leap years actual/actual",
			args{
				SimpleDate("2015-01-15"),
				SimpleDate("2017-06-15"),
				Act365,
			},
			(350 + 365 + 167) / 365.,
		},
		{
			"Long mixed leap years actual/actual",
			args{
				SimpleDate("2018-01-15"),
				SimpleDate("2020-06-15"),
				ActAct,
			},
			(350 / 365.) + (365 / 365.) + (167 / 366.),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DayCountFactor(tt.args.accrualStart, tt.args.accrualEnd, tt.args.basis); got != tt.want {
				t.Errorf("DayCountFactor() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dayDiff(t *testing.T) {
	type args struct {
		start time.Time
		end   time.Time
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			"Same day",
			args{
				SimpleDate("2015-07-15"),
				SimpleDate("2015-07-15"),
			},
			0.,
		},
		{
			"Tomorrow",
			args{
				SimpleDate("2015-07-15"),
				SimpleDate("2015-07-16"),
			},
			1.,
		},
		{
			"Different day",
			args{
				SimpleDate("2020-01-15"),
				SimpleDate("2020-06-15"),
			},
			152.,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := dayDiff(tt.args.start, tt.args.end); got != tt.want {
				t.Errorf("dayDiff() = %v, want %v", got, tt.want)
			}
		})
	}
}
