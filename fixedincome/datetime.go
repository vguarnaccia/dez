package fixedincome

import (
	"log"
	"math"
	"time"
)

// LastDayOfMonth is the least frustrating way of getting the last day of each
// month in Go.
var LastDayOfMonth = map[time.Month]int{
	time.January:   31,
	time.February:  28,
	time.March:     31,
	time.April:     30,
	time.May:       31,
	time.June:      30,
	time.July:      31,
	time.August:    31,
	time.September: 30,
	time.October:   31,
	time.November:  30,
	time.December:  31,
}

// SimpleDate recreates a time from "YYYY-MM-DD"
func SimpleDate(date string) time.Time {
	datetime, err := time.Parse("2006-01-02", date)
	if err != nil {
		log.Panicf("Failed to convert string %v to date", date)
	}
	return datetime
}

func isoDates(dates ...string) []time.Time {
	results := make([]time.Time, 0)
	for _, date := range dates {
		results = append(results, SimpleDate(date))
	}
	return results
}

// DayCountFactor returns the amount of the coupon rate applied when calculating
// interest.
//
// Note, frequency is ignored for 30/360 day counts and PaymentDate is
// typically the same as AccrualEnd.
func DayCountFactor(accrualStart, accrualEnd time.Time, basis DayCountConvention) float64 {
	y1, m1, d1 := accrualStart.Date()
	Y1, M1, D1 := float64(y1), float64(m1), float64(d1)

	y2, m2, d2 := accrualEnd.Date()
	Y2, M2, D2 := float64(y2), float64(m2), float64(d2)

	switch basis {
	case Thirty360:
		D1 = math.Max(D1, 30)
		D2 = math.Max(D2, 30)
		return (360*(Y2-Y1) + 30*(M2-M1) + (D2 - D1)) / 360
	case ActAct:
		if Y1 == Y2 {
			return dayDiff(accrualStart, accrualEnd) / daysInYear(accrualStart)
		}
		nextYearEnd := endOfYear(accrualStart)
		daysToNextEnd := dayDiff(accrualStart, nextYearEnd) / daysInYear(accrualStart)

		lastYearEnd := endOfPreviousYear(accrualEnd)
		daysAfterLastEnd := dayDiff(lastYearEnd, accrualEnd) / daysInYear(accrualEnd)

		return (Y2 - Y1 - 1) + daysToNextEnd + daysAfterLastEnd
	case Act360:
		return dayDiff(accrualStart, accrualEnd) / 360
	case Act365:
		return dayDiff(accrualStart, accrualEnd) / 365
	}
	log.Panicf("Unsupported day count convention called: %v", basis)
	return -1
}

// EDate replicates Excel's EDATE function for adding or subtracting months.
// Returned date has a day less than or equal to input date.
//
// Be advised moving forward and backward the same number of month may yield
// different dates.
func EDate(date time.Time, months int) time.Time {
	newDate := date.AddDate(0, months, 0)

	// Check if we fell into next month.
	if newDate.Day() != date.Day() {
		return newDate.AddDate(0, 0, -newDate.Day())
	}
	return newDate
}

// EndOfMonth replicates Excel's EOMonth function. It's similar to EDate but
// it always returns the end of the month.
func EndOfMonth(date time.Time, months int) time.Time {
	oneTooFar := EDate(date, months+1)
	return oneTooFar.AddDate(0, 0, -oneTooFar.Day())
}

func dayDiff(start, end time.Time) float64 {
	return end.Sub(start).Hours() / 24.
}

// daysInYear returns 366 if date is in leap year, otherwise 365.
func daysInYear(date time.Time) float64 {
	year := date.Year()
	if (year%4 == 0 && year%100 != 0) || year%400 == 0 {
		return 366
	}
	return 365
}

func endOfYear(date time.Time) time.Time {
	return time.Date(date.Year(), time.December, 31, 0, 0, 0, 0, date.Location())
}

func endOfPreviousYear(date time.Time) time.Time {
	return time.Date(date.Year()-1, time.December, 31, 0, 0, 0, 0, date.Location())
}
